#!/bin/bash

DISK_USAGE=`df -h | grep -v tmpfs`
MY_IP=`ip addr|grep -v 127.0.0.1 | grep inet|grep -v inet6|cut -d ' ' -f 6|cut -d '/' -f 1`
MY_NAME=`hostname`
RUN_DATE=`date +%m-%d-%Y_%H:%M`

#### EMAIL SETTINGS ###
SMTP_SERVER="<your email relay here>"
FROM="Whatever<youwant@example.com>"
TO="<your real email>"
SUBJECT="$MY_NAME ($MY_IP) Disk Usage Stats"

#echo "Date: $RUN_DATE"
#echo "Host: $MY_NAME"
#echo "IP  : $MY_IP"
#echo "======================"
#echo "  DISK USAGE REPORT   "
#echo "======================"
#echo " $DISK_USAGE"

BODY="
===================================================
$MY_NAME($MY_IP) DISK USAGE
===================================================
$DISK_USAGE
"

echo "$BODY" | mail -S "smtp=$SMTP_SERVER" -s "$SUBJECT" -r $FROM  $TO

echo "Mail Sent."
