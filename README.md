# linux-utils

Various linux Bash / Shell scripts for making life easier.

## samba/user-connection

This script takes the name of the SAMBA share as the only argument, and returns a list of uses that are connected.