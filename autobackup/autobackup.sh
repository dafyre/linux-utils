### SCRIPT PARAMETERS AND SELF CONFIGURATION ###
BACKUP_USER='remote_user'  #Set up Key Based Authentication  
BACKUP_HOST="remote_host"
BACKUP_KEY='/root/.ssh/backup.key' #Path to key file
HAS_MYSQL="/usr/bin/mysql"
MYNAME=`hostname`
BACKUP_DEST="/somefolder/$MYNAME"
BACKUP_SOURCE="/web"
### SET THESE IF MYSQL REQUIRES AUTHENTICATION ###
MYSQL_USER=
MYSQL_PASS=
###

FULL_PATH="$(readlink -f ${BASH_SOURCE})"
SCRIPT_PATH=$(dirname $FULL_PATH)

echo "Setting Backup name to $MYNAME"


if [ -f $HAS_MYSQL ]; then
  HAS_MYSQL=1
  echo "Has Mysql Says: $HAS_MYSQL"
else
  echo "No Mysql installed!"
fi

##Set Up Backup Destination
echo "Setting Backup Destination"


echo "Backup Destination is $BACKUP_HOST:$BACKUP_DEST"

echo "Setting up local folders..."

mkdir -p /web/backup/out/old

### END SCRIPT PARAMETERS AND SELF CONFIGURATION

### BACKUP SCRIPT STARTS HERE

#!/bin/bash

echo ""
echo "***********************************************************************"
date
echo ""

cd $BACKUP_SOURCE


dt1=`date +%m-%d-%Y`

pushd $BACKUP_SOURCE

tar -chf $BACKUP_SOURCE/$MYNAME\_web-$dt1.tar --exclude='phpMyAdmin/*' --exclude='phpmyadmin/*' --exclude='main/*' --exclude='backup/out/old*' --exclude='backup/$MYNAME*' *
popd

pushd /etc
tar -chf $BACKUP_SOURCE/$MYNAME\_etc-$dt1.tar *
popd

pushd $BACKUP_SOURCE

if [ $HAS_MYSQL == 1 ]; then
  echo "Backing up MySQL Databases"
  if [ -z $MYSQL_USER ]; then
    echo "Attempting log in as root without password"
    /usr/bin/mysqldump --all-databases > $MYNAME\_db-$dt1.sql
   else
    echo "Logging in as $MYSQL_USER"
    /usr/bin/mysqldump -u $MYSQL_USER -p$MYSQL_PASS --all-databases > $MYNAME\_db-$dt1.sql
  fi

fi

tar -cf $MYNAME-$dt1.tar $MYNAME\_*.tar $MYNAME\_db*.sql

rm -f $MYNAME\_*.tar $MYNAME\_db*.sql

gzip *.tar
mv *.gz out

popd


echo "Transferring data to $BACKUP_HOST"

ssh -i $BACKUP_KEY  $BACKUP_USER@$BACKUP_HOST mkdir -p $BACKUP_DEST
scp -i $BACKUP_KEY -r $BACKUP_SOURCE/out/*.gz $BACKUP_USER@$BACKUP_HOST:$BACKUP_DEST/ &&
rm $BACKUP_SOURCE/out/old/*.gz &&
mv $BACKUP_SOURCE/*.gz $BACKUP_SOURCE/out/old


echo ""
date
echo ""
echo "***********************************************************************"

### END BACKUP SCRIPT ###
